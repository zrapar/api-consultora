﻿# Api Consultora Higiene y Seguridad

API rest a ser consumida por la aplicación en React, para la gestión y administración de la consultora industrial Jose Parodi.


# Deploy

La aplicación esta dockerizada, solo corre este comando para crear y correr la imagen de docker en sus respectivos contenedores

    docker-compose up -d
Luego se procede a migrar la base de datos.

    docker-compose exec app python3 /code/manage.py migrate --no-input
por cambios en la nueva version de MySQL, hay que correr los siguientes comandos (o bien ejecutar la sentencia mediante navicat o el manejador de base de datos de tu preferencia)

    docker-compose exec db mysql -uroot -psecret
    ALTER USER 'root'IDENTIFIED WITH mysql_native_password BY 'secret123';
