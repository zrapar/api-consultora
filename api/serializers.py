from django.contrib.auth.hashers import make_password
from rest_framework import serializers

from api.models import Usuarios, Firmante, Cliente, User, Estudios, Tarea, Informe


class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ['username', 'first_name', 'last_name', 'email', 'password']

	def create(self, validated_data):
		user = User.objects.create(
			username=validated_data['username'],
			first_name=validated_data['first_name'],
			last_name=validated_data['last_name'],
			email=validated_data['email'],
			password=make_password(validated_data['password'])
		)

		return user

class ClienteShortSerializer(serializers.ModelSerializer):
	class Meta:
		model = Cliente
		fields = ['id', 'empresa']


class FirmanteSerializer(serializers.ModelSerializer):
	# cliente = ClienteShortSerializer()

	class Meta:
		model = Firmante
		fields = ['id', 'full_name', 'dni', 'phone', 'email', 'cliente']


class FirmanteShortSerializer(serializers.ModelSerializer):
	class Meta:
		model = Firmante
		fields = ['id']


class UsuariosSerializer(serializers.ModelSerializer):
	class Meta:
		model = Usuarios
		fields = ['id', 'name', 'incunvencias', 'titulo1', 'titulo2', 'matricula1', 'matricula2', 'matricula3',
		          'matricula4', 'matricula5', 'dni', 'telefono', 'direccion']


class UsuariosShortSerializer(serializers.ModelSerializer):
	class Meta:
		model = Usuarios
		fields = ['id', 'name']


class EstudiosSerializer(serializers.ModelSerializer):
	class Meta:
		model = Estudios
		fields = ['id', 'name', 'tipo']


class TareaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Tarea
		fields = "__all__"


class ClienteSerializer(serializers.ModelSerializer):
	class Meta:
		model = Cliente
		fields = ['id', 'empresa', 'direccion', 'cuit', 'id_estab', 'zip_code', 'phone',
		          'email', 'user_opds', 'user_acumar',
		          'user_ina', 'user', 'clave_opds',
		          'clave_acumar', 'clave_ina', 'clave']


class InformeSerializer(serializers.ModelSerializer):
	class Meta:
		model = Informe
		fields = ['id', 'informe', 'tarea', 'numero_remito', 'fecha_entrega']

	def create(self, validated_data):
		last = Informe.objects.last()

		obj = Informe.objects.create(
			informe=validated_data['informe'],
			tarea=validated_data['tarea'],
			numero_remito=last.numero_remito + 1,
			fecha_entrega=validated_data['fecha_entrega'],
		)
		task = Tarea.objects.get(id=obj.tarea.id)
		if validated_data['fecha_entrega']:
			task.estado = "Informe Entregado"

		else:
			task.estado = "Informe Cargado"
		task.save()

		return obj

	def update(self, instance, validated_data):
		task = Tarea.objects.get(id=instance.tarea.id)
		instance.informe = validated_data.get('informe', instance.informe)
		instance.tarea = validated_data.get('tarea', instance.tarea)
		instance.numero_remito = validated_data.get('numero_remito', instance.numero_remito)
		instance.fecha_entrega = validated_data.get('fecha_entrega', instance.fecha_entrega)
		if validated_data['fecha_entrega']:
			task.estado = "Informe Entregado"

		else:
			task.estado = "Informe Cargado"
		task.save()
		instance.save()

		return instance







'''class UserSerializer(serializers.ModelSerializer):
    
    profile = ProfileSerializer(required=True)
    class Meta:
        model = User
        fields = ('url', 'email', 'profile', 'created',)

    def create(self, validated_data):

        # create user
        user = User.objects.create(
            url = validated_data['url'],
            email = validated_data['email'],
            # etc ...
        )

        profile_data = validated_data.pop('profile')
        # create profile
        profile = Profile.objects.create(
            user = user
            first_name = profile_data['first_name'],
            last_name = profile_data['last_name'],
            # etc...
        )

        return user'''


class ChangePasswordSerializer(serializers.Serializer):
	model = User

	"""
    Serializer for password change endpoint.
    """
	old_password = serializers.CharField(required=True)
	new_password = serializers.CharField(required=True)
