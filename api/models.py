from django.contrib.auth.models import User
from django.db import models


class Usuarios(models.Model):
    """user = models.EmbeddedModelField(
        model_container=User
    )"""
    name = models.CharField(max_length=50)
    incunvencias = models.CharField(max_length=50)
    titulo1 = models.CharField(max_length=20)
    titulo2 = models.CharField(max_length=20)
    matricula1 = models.CharField(max_length=300)
    matricula2 = models.CharField(max_length=300)
    matricula3 = models.CharField(max_length=300)
    matricula4 = models.CharField(max_length=300)
    matricula5 = models.CharField(max_length=300)
    dni = models.CharField(max_length=50)
    telefono = models.CharField(max_length=25)
    direccion = models.CharField(max_length=300)

    def __str__(self):
        return self.name


class Cliente(models.Model):
    empresa = models.CharField(max_length=50)
    direccion = models.CharField(max_length=150)
    cuit = models.CharField(max_length=10)
    id_estab = models.CharField(max_length=10)
    zip_code = models.CharField(max_length=10)
    phone = models.CharField(max_length=20)
    email = models.CharField(max_length=20)
    user_opds = models.CharField(max_length=20)
    user_acumar = models.CharField(max_length=20)
    user_ina = models.CharField(max_length=20)
    user = models.CharField(max_length=20)
    clave_opds = models.CharField(max_length=20)
    clave_acumar = models.CharField(max_length=20)
    clave_ina = models.CharField(max_length=20)
    clave = models.CharField(max_length=20)
    # planos = models.ArrayReferenceField()

    def __str__(self):
        return self.empresa


class Firmante(models.Model):
    full_name = models.CharField(max_length=20)
    dni = models.CharField(max_length=20)
    phone = models.CharField(max_length=20)
    email = models.CharField(max_length=20)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)

    def __str__(self):
        return self.full_name


class Estudios(models.Model):
    ESTUDIOS_CHOICES = [
        ('F', 'Fisico'),
        ('Q', 'Quimico'),
    ]
    name = models.CharField(max_length=30)
    tipo = models.CharField(max_length=1, choices=ESTUDIOS_CHOICES)

    def __str__(self):
        return self.name


"""class Constantes(models.Model):

    cliente = models.ArrayModelField(
        model_container=Cliente,
    )"""


class Tarea(models.Model):
    estudio = models.ForeignKey(Estudios, on_delete=models.CASCADE)
    responsable = models.ForeignKey(Usuarios, on_delete=models.CASCADE)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    inicio = models.DateTimeField(editable=True)
    fin = models.DateTimeField(editable=True)
    estado = models.CharField(max_length=50)

    def __str__(self):
        return self.estudio.name + '/' + self.cliente.empresa


class Informe(models.Model):
    fecha_carga = models.DateTimeField(auto_now_add=True)
    informe = models.FilePathField(path='api/informes')
    tarea = models.ForeignKey(Tarea, on_delete=models.DO_NOTHING)
    numero_remito = models.IntegerField(null=True)  # TODO: definir un default para que matche el ID de ellos.
    fecha_entrega = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.tarea.__str__()


class Calibracion(models.Model):
    estudio = models.ForeignKey(Estudios, on_delete=models.CASCADE)
    instrumento = models.CharField(max_length=50)
    fecha = models.DateTimeField()
    certificado = models.FilePathField(path='api/calibraciones')

    def __str__(self):
        return self.instrumento

# Create your models here.
