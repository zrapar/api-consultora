from django.contrib import admin

from .models import Cliente, Estudios, Usuarios, Tarea, Firmante, Calibracion, Informe

admin.site.register(Cliente)
admin.site.register(Estudios)
admin.site.register(Usuarios)
admin.site.register(Tarea)
admin.site.register(Firmante)
admin.site.register(Calibracion)
admin.site.register(Informe)

# Register your models here.
