"""api_consultora URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from rest_framework.routers import DefaultRouter
from rest_framework_extensions.routers import NestedRouterMixin

from api.views import ClientesViewSet, FirmantesViewSet, TareasViewSet, UserViewSet, EstudiosViewSet, UsuariosViewSet, \
	InformesViewSet


class NestedDefaultRouter(NestedRouterMixin, DefaultRouter):
	pass


router = NestedDefaultRouter()

clientes_router = router.register('clientes', ClientesViewSet)
clientes_router.register(
	'firmantes',
	FirmantesViewSet,
	basename='cliente-firmante',
	parents_query_lookups=['cliente'],
)
clientes_router.register(
	'tareas',
	TareasViewSet,
	basename='tarea-estudio',
	parents_query_lookups=['cliente']
)
estudios_router = router.register('estudios', EstudiosViewSet)
estudios_router.register(
	'tareas',
	TareasViewSet,
	basename='tarea-estudio',
	parents_query_lookups=['estudio'],
)

usuarios_router = router.register('usuarios', UsuariosViewSet)
usuarios_router.register(
	'tareas',
	TareasViewSet,
	basename='tarea-estudio',
	parents_query_lookups=['responsable'],

)
router.register(r'tareas', TareasViewSet, basename='tarea')
router.register(r'user', UserViewSet, basename='user')
router.register(r'firmantes', FirmantesViewSet, basename='firmantes')
router.register(r'informes', InformesViewSet, basename='informes')



urlpatterns = router.urls
