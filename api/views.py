from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.http import JsonResponse
from rest_framework import viewsets, status
from rest_framework.generics import UpdateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import mixins, GenericViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin

from api.serializers import FirmanteSerializer, ClienteSerializer, ChangePasswordSerializer, \
    UsuariosSerializer, TareaSerializer, UserSerializer, EstudiosSerializer, InformeSerializer
from .models import Firmante, Usuarios, Cliente, Tarea, Estudios, Informe


def SendMail(request):
    send_mail(
        'Titulo',
        'Contenido',
        'prueba@prueba.com',
        ['fabclub747@gmail.com'],
        fail_silently=False,
    )
    response = {
        'status': 'success',
        'code': status.HTTP_200_OK,
        'message': 'Correo mandado exitosamente',
        'data': []
    }
    return JsonResponse(response)


'''class FirmantesViewSet(viewsets.ViewSet):

    def list(self, request):
        queryset = Firmante.objects.all()
        serializer = FirmanteSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Firmante.objects.all()
        firmante = get_object_or_404(queryset, pk=pk)
        serializer = FirmanteSerializer(firmante)
        return Response(serializer.data) '''


class UsuariosViewSet(NestedViewSetMixin, viewsets.ModelViewSet):  # TODO: vincular a modelo USER
    serializer_class = UsuariosSerializer
    queryset = Usuarios.objects.all()


class InformesViewSet(NestedViewSetMixin, mixins.ListModelMixin, mixins.CreateModelMixin, mixins.RetrieveModelMixin,
                      mixins.UpdateModelMixin, GenericViewSet):
    serializer_class = InformeSerializer
    queryset = Informe.objects.all()


class EstudiosViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    serializer_class = EstudiosSerializer
    queryset = Estudios.objects.all()


class ClientesViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    serializer_class = ClienteSerializer
    queryset = Cliente.objects.all()


class ClienteFromFirmante(NestedViewSetMixin, mixins.RetrieveModelMixin, GenericViewSet):

	serializer_class = ClienteSerializer
	queryset = Cliente.objects.all()


class TareasViewSet(NestedViewSetMixin, mixins.ListModelMixin, mixins.CreateModelMixin, mixins.RetrieveModelMixin,
                       mixins.UpdateModelMixin, mixins.DestroyModelMixin, GenericViewSet):
    serializer_class = TareaSerializer
    queryset = Tarea.objects.all()


class FirmantesViewSet(NestedViewSetMixin, mixins.ListModelMixin, mixins.CreateModelMixin, mixins.RetrieveModelMixin,
                          mixins.UpdateModelMixin, mixins.DestroyModelMixin, GenericViewSet):
    serializer_class = FirmanteSerializer
    queryset = Firmante.objects.all()


class UserViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()


class ClienteFirmanteViewSet(NestedViewSetMixin, viewsets.ViewSet):
    queryset = Cliente.objects.all()

    '''def list(self, request, cliente_pk=None):
        queryset = self.queryset.filter(cliente=cliente_pk)
        serializer = ClienteSerializer(queryset, many=True)
        return Response(serializer.data)'''

    def retrieve(self, request, pk=None, cliente_pk=None):
        queryset = self.queryset.get(id=cliente_pk)
        serializer = ClienteSerializer(queryset)
        return Response(serializer.data)


class ChangePasswordView(UpdateAPIView):
    serializer_class = ChangePasswordSerializer
    model = User
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password updated successfully',
                'data': []
            }

            return Response(response)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
